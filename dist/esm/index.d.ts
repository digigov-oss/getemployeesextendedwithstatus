import { AuditRecord, AuditEngine } from '@digigov-oss/gsis-audit-record-db';
export declare type errorRecord = {
    errorCode: string;
    errorDescr: string;
};
export declare type getEmployeesExtendedWithStatusInputRecord = {
    extendedAfm: string;
};
export declare type employmentInfos = {
    organicOrganizationId: string;
    organicOrganizationName: string;
    workOrganizationId?: string;
    workOrganizationName?: string;
    organizationName?: string;
    employmentTypeCategory: string;
    staffCategory: string;
    employmentTypeId: string;
    employmentTypeDescription: string;
    employeeCategoryId: string;
    employeeCategoryDescription: string;
    hireDate: string;
    contractEndDate: string;
    primary: string;
    supervisor: string;
    supervisorPositionId: string;
    supervisorPositionDescription: string;
};
export declare type getEmployeesOutputRecord = {
    vatId: string;
    lastName: string;
    firstName: string;
    fathersFirstName: string;
    employmentInfos: {
        extendedItem: employmentInfos[];
    };
    callSequenceId: string;
    callSequenceDate: string;
    errorRecord: errorRecord;
};
export declare type getEmployeesExtendedWithStatusOutputRecord = {
    data: getEmployeesOutputRecord;
};
/**
 * @type Overrides
 * @description Overrides for the SOAP client
 * @param {string} endpoint - The endpoint to connect to
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 */
export declare type overrides = {
    endpoint?: string;
    prod?: boolean;
    auditInit?: AuditRecord;
    auditStoragePath?: string;
    auditEngine?: AuditEngine;
};
/**
 *
 * @param input getEmployeesExtendedWithStatusInputRecord;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns getEmployeesExtendedWithStatusOutputRecord | errorRecord
 */
export declare const getInfo: (input: getEmployeesExtendedWithStatusInputRecord, user: string, pass: string, overrides?: overrides | undefined) => Promise<any>;
export default getInfo;
