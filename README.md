# getEmployeesExtendedWithStatus

Client to connect on GSIS service.

Returns extensive civil service work data.

### Example
```
import getInfo from '@digigov-oss/get-employees-extended-with-status';
import config from './config.json'; 

const test = async () => {
   
   const input = {
         extendedAfm: config.afm // replace with valid real afm of employee!
   }
   
    try {
        const Info = await getInfo(input, config.user, config.pass);
        console.log(Info); 
    } catch (error) {
        console.log(error);
    }
}

test();
```

* you can use `overrides` to override the default values for auditrecord and input fields.
* for your tests, you don't need to use the `overrides` mechanism, in that case, the default storage path will be used ie `/tmp`
* look at [KED](https://www.gsis.gr/dimosia-dioikisi/ked/) standard guides for records you can use on auditInit"
Also, you can use `overrides` to override the default storage engine. Look at module [AuditRecordDB](https://gitlab.grnet.gr/digigov-oss/auditRecordDB/-/blob/main/README.md) for more details on how to use the AuditEngine.

#### Returns
an object like the following:
```
{
        getEmployeesExtendedWithStatusOutputRecord: {
                data: {
                        vatId: 'ΑΦΜ',
                        lastName: 'ΕΠΩΝΥΜΟ',
                        firstName: 'Όνομα Υπαλ',
                        fathersFirstName: 'Όνομα Πατέρα',
                        employmentInfos: {
                                extendedItem: [
                                        {
                                                organicOrganizationId: '80640',
                                                organicOrganizationName: 'ΕΙΔΙΚΟΣ ΛΟΓΑΡΙΑΣΜΟΣ ΚΟΝΔΥΛΙΩΝ ΕΡΕΥΝΑΣ Ε.Κ.Ε.Φ.Ε "ΔΗΜΟΚΡΙΤΟΣ"',
                                                workOrganizationId: '80640',
                                                organizationName: 'ΕΙΔΙΚΟΣ ΛΟΓΑΡΙΑΣΜΟΣ ΚΟΝΔΥΛΙΩΝ ΕΡΕΥΝΑΣ Ε.Κ.Ε.Φ.Ε "ΔΗΜΟΚΡΙΤΟΣ"',
                                                employmentTypeCategory: 'fixed_term',
                                                staffCategory: 'temporary',
                                                employmentTypeId: 37,
                                                employmentTypeDescription: 'ΣΥΜΒΑΣΙΟΥΧΟΙ ΕΡΓΟΥ /ΧΡΗΜΑΤΟΔΟΤΟΥΜΕΝΑ ΜΕΣΩ ΕΣΠΑ Ή ΑΠΟ ΙΔΙΟΥΣ ΠΟΡΟΥΣ',
                                                employeeCategoryId: 27,
                                                employeeCategoryDescription: 'ΛΟΙΠΟ ΕΚΤΑΚΤΟ ΠΡΟΣΩΠΙΚΟ (ΠΛΗΝ ΚΑΛΛΙΤΕΧΝΙΚΟ)',
                                                hireDate: '2019-07-05',
                                                contractEndDate: '2019-09-04',
                                                primary: false,
                                                supervisor: false,
                                                supervisorPositionId: 8,
                                                supervisorPositionDescription: 'ΧΩΡΙΣ ΘΕΣΗ ΕΥΘΥΝΗΣ'
                                        },
                                ]
                        }
                }
        },
        callSequenceId: 50155765,
        callSequenceDate: Thu Jun 23 2022 14:10:55 GMT+0300 (Θερινή ώρα Ανατολικής Ευρώπης),
        errorRecord: null,
        auditUnit: 'gov.gr',
        auditTransactionId: '17',
        auditProtocol: '17/2022-06-23',
        auditTransactionDate: '2022-06-23T11:10:54Z',
        auditUserIp: '127.0.0.1',
        auditUserId: 'system'
}
```
* Please note: on test the `lastName`,`firstName`,`fathersFirstName` are always returned with mock values....

or an error object like the following:
```
{
        callSequenceId: 50193758,
        callSequenceDate: Thu Jun 23 2022 14:20:49 GMT+0300 (Θερινή ώρα Ανατολικής Ευρώπης),
        errorRecord: {
                errorCode: 'GEN_INVALID_DATA',
                errorDescr: 'Το πεδίο afm δεν είναι έγκυρος Αριθμός Φορολογικού Μητρώου'
        },
        auditUnit: 'gov.gr',
        auditTransactionId: '18',
        auditProtocol: '18/2022-06-23',
        auditTransactionDate: '2022-06-23T11:20:48Z',
        auditUserIp: '127.0.0.1',
        auditUserId: 'system'
}
```
### Notes
In case of KED advertises wrong endpoint on production you have to use (override) the endpoint: `https://ked.gsis.gr/esb/apografiEmployeesService`
You can do that by setting the `endpoint` property on the `overrides` object.
```
const overrides = {
    endpoint: 'https://ked.gsis.gr/esb/apografiEmployeesService',
}
```

