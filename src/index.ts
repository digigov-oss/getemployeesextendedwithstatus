import soapClient from './soapClient.js';
import {generateAuditRecord, AuditRecord, FileEngine, AuditEngine } from '@digigov-oss/gsis-audit-record-db';
import config from './config.json'; 

export type errorRecord = {
    errorCode:string;
    errorDescr:string;
}

export type getEmployeesExtendedWithStatusInputRecord = {
    extendedAfm:string;
}

export type employmentInfos = {
    organicOrganizationId:string;
    organicOrganizationName:string;
    workOrganizationId?:string;
    workOrganizationName?:string; //this probably never happens 
    organizationName?:string; //this is not documented in the Doc provided from KED....
    employmentTypeCategory:string;
    staffCategory:string;
    employmentTypeId:string;
    employmentTypeDescription:string;
    employeeCategoryId:string;
    employeeCategoryDescription:string;
    hireDate:string;
    contractEndDate:string;
    primary:string;
    supervisor:string;
    supervisorPositionId:string;
    supervisorPositionDescription:string;
}

export type getEmployeesOutputRecord = {
    vatId:string;
    lastName:string; //TODO check on production... On tests this does not return the real value
    firstName:string;// same as above
    fathersFirstName:string;// same as above
    employmentInfos:{
        extendedItem:employmentInfos[];
    }
    callSequenceId:string;
    callSequenceDate:string;
    errorRecord:errorRecord;
}

export type getEmployeesExtendedWithStatusOutputRecord = {
    data:getEmployeesOutputRecord;
}

/**
 * @type Overrides
 * @description Overrides for the SOAP client
 * @param {string} endpoint - The endpoint to connect to
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 */
 export type overrides = {
    endpoint?:string;
    prod?:boolean;
    auditInit?: AuditRecord;
    auditStoragePath?: string;
    auditEngine?: AuditEngine;
 }

/**
 * 
 * @param input getEmployeesExtendedWithStatusInputRecord;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns getEmployeesExtendedWithStatusOutputRecord | errorRecord
 */
 export const getInfo = async (input:getEmployeesExtendedWithStatusInputRecord, user:string, pass:string, overrides?:overrides) => {
    const endpoint = overrides?.endpoint ?? "";
    const prod = overrides?.prod ?? false;
    const auditInit = overrides?.auditInit ?? {} as AuditRecord;
    const auditStoragePath = overrides?.auditStoragePath ?? "/tmp"
    const auditEngine = overrides?.auditEngine ?? new FileEngine(auditStoragePath);
    const wsdl = prod==true? config.prod.wsdl : config.test.wsdl;
    const auditRecord = await generateAuditRecord(auditInit, auditEngine);
    if (!auditRecord) throw new Error('Audit record is not initialized');
    try {
        const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
        const Info = await s.getInfo(input);
        return {...Info,...auditRecord};
       } catch (error) {
           throw(error);
       }
}
export default getInfo;
